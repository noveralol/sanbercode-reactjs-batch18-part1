// Soal 1

console.log("LOOPING PERTAMA");

let angkaPertama= 2;
while(angkaPertama <= 20){
  console.log(angkaPertama + " - I love coding");
  angkaPertama+=2;
}

console.log("LOOPING KEDUA");
while (angkaPertama > 2) {
  console.log((angkaPertama-2) + " - I will become a frontend developer");
  angkaPertama-=2;
};

// Soal 2

let soalDua = 1;
for ( soalDua;soalDua <= 20; soalDua++){
  if(soalDua%2==0){ 
    console.log(soalDua + " - Berkualitas")
  } else if(soalDua%3==0) {
    console.log(soalDua + " - I Love Coding")
  } else {
    console.log(soalDua + " - Santai")
  }
};

// Soal 3

var txt ="";
for (let a = 1; a <= 7; a++) {
  for(let b = 1; b <= a; b++){
  txt += "#";
}
  txt +="\n";
}
console.log(txt);

// Soal 4

var kalimat = "saya sangat senang belajar javascript"
console.log(kalimat.split(" "));

// Soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
for(let awal= 0; awal < daftarBuah.sort().length; awal++){
  console.log(daftarBuah[awal] + "\n");
};
