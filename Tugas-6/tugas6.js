// Soal 1

const luasLingkaran = () => {
  let piw = 3.14; 
 return piw * Math.pow(5,2)};

 const kelilingLingkaran = () => {
  let piw = 3.14;
  return 2 * piw * 3 ;
};
console.log(luasLingkaran() + "\n" + kelilingLingkaran());

// Soal 2

let kalimat = "";
const tambahKata = (kata) => `${kata}`;
kalimat += tambahKata`saya `;
kalimat += tambahKata`adalah `;
kalimat += tambahKata`seorang `;
kalimat += tambahKata`frontend `;
kalimat += tambahKata`developer`;
console.log(kalimat);


// Soal 3

const newFunction = function literal(firstName, lastName) {
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
    }
  }
};

newFunction("William", "Imoh").fullName();

// Soal 4

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
};

const { firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);

// Soal 5

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east];

console.log(combined);