// di index.js
var readBooks = require('./callback');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
  { name: 'komik', timeSpent: 1000 }
];

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function (nextBooks) {
  readBooks(nextBooks, books[1], function (nextBooks2) {
    readBooks(nextBooks2, books[2], function (nextBooks3) {
      readBooks(nextBooks3, books[3], function (nextBooks4) {
        console.log(nextBooks4);
      });
    });
  });
});