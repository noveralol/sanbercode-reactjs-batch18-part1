var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

readBooksPromise(10000, books[0]).then(function(nextBooks){
  readBooksPromise(nextBooks, books[1]).then(function (nextBooks2) {
    readBooksPromise(nextBooks2, books[2]).then(function (nextBooks3){
      console.log(nextBooks3);
    }
   )
  }
 )
});